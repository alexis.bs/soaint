function mergeAndSort(arr1, arr2) {
    const merged = [...arr1, ...arr2];
    const uniqueValues = Array.from(new Set(merged));
    const sortedResult = uniqueValues.sort((a, b) => a - b);
    return sortedResult;
}

// Ejemplo de uso
const array1 = [3, 1, 5, 2, 3, 8];
const array2 = [4, 2, 7, 5, 9];
const resultado = mergeAndSort(array1, array2);
console.log(resultado);
