# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 0.3.0 (2023-08-17)


### Features

* add controller of user ([79c8a33](https://gitlab.com/alexis.bs/soaint/commit/79c8a33ea713f7ea1e44fb632704f63d1300b528))
* add files of configuration ([cfd180d](https://gitlab.com/alexis.bs/soaint/commit/cfd180d31deed98e918cce74bbadc4a77e605449))
* add global variables ([ee69c0a](https://gitlab.com/alexis.bs/soaint/commit/ee69c0a5e2b35575dd3d2091e9c613170b5ed79d))
* add helper validator ([ed3f679](https://gitlab.com/alexis.bs/soaint/commit/ed3f679e18f1eff393fbf363d21f6e437bf0a9fb))
* add interface of user ([efd8405](https://gitlab.com/alexis.bs/soaint/commit/efd8405ff88332e1e28d346d979291a4c6233444))
* add module of shared ([ebeb025](https://gitlab.com/alexis.bs/soaint/commit/ebeb025d27ea30e750c8bb82323b810c01552f7b))
* add module of user ([4526d12](https://gitlab.com/alexis.bs/soaint/commit/4526d12adda58262775ed293f215325247220167))
* add module primary ([9f57522](https://gitlab.com/alexis.bs/soaint/commit/9f5752222f4786254927b6a9bcd2296e414599e3))
* add schema of user ([17a9d20](https://gitlab.com/alexis.bs/soaint/commit/17a9d20e39903519568d01c0bcb7b9d7d05accf4))
* add service of get info ([c1f5301](https://gitlab.com/alexis.bs/soaint/commit/c1f530116ed3f79b7be1ce6bd9ffc3f318fdbf6d))
* add service of upload file and register users ([3f915c6](https://gitlab.com/alexis.bs/soaint/commit/3f915c609dce6f201e9b43824f1e5a36cff2a992))
* files of test ([9732557](https://gitlab.com/alexis.bs/soaint/commit/9732557c6631ef6c1112c5e2bb7245c473ff9018))
