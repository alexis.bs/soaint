export enum CaptureHeaderTypes {
  AGE = 'age',
  ID = 'id',
  GENDER = 'gender',
  NAME = 'name',
}

export enum CaptureGenderTypes {
  M = 'M',
  F = 'F',
}
