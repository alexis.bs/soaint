export interface newUserInterface {
  name: string;
  age: number;
  gender: string;
  idNumber: number;
}
export const UserSessionName = 'user-sessions';
