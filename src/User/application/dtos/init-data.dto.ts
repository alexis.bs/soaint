import { IsNotEmpty } from 'class-validator';

export class InitDataDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  age: number;

  @IsNotEmpty()
  gender: Date;

  @IsNotEmpty()
  idNumber: number;
}
