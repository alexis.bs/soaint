import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { UserSessionName } from '../domain/entities/user-interface';
import { UserDocument } from '../infrastructure/controllers/persistence/user.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class GetUserService {
  private readonly logger = new Logger(GetUserService.name);
  constructor(
    @InjectModel(UserSessionName) private userModel: Model<UserDocument>,
  ) {}

  async run(id: number): Promise<any> {
    const user = await this.userModel
      .findOne({ idNumber: id })
      .catch((error) => {
        this.logger.error(error.message, error.stack);
        throw new InternalServerErrorException(error.message);
      });
    if (user) {
      return user["_doc"];
    }
    throw new BadRequestException('user not exist');
  }
}
