import {
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { UserSessionName } from '../domain/entities/user-interface';
import { UserDocument } from '../infrastructure/controllers/persistence/user.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
@Injectable()
export class CreateUsersService {
  private readonly logger = new Logger(CreateUsersService.name);
  constructor(
    @InjectModel(UserSessionName) private userModel: Model<UserDocument>,
  ) {}

  async run(file: Express.Multer.File): Promise<string> {
    const lines = file.buffer.toString().split('\n');

    lines.slice(1).forEach(async (line) => {
      const [id, name, age, gender] = line.split(',');
      if (
        id != undefined &&
        name != undefined &&
        gender != undefined &&
        gender != undefined
      ) {
        const user = await this.userModel.findOne({ idNumber: id });
        if (!user) {
          const data = {
            idNumber: parseInt(id),
            name,
            age: parseInt(age),
            gender,
          };

          await this.userModel.create(data).catch((error) => {
            this.logger.error(error.message, error.stack);
            throw new InternalServerErrorException();
          });
          this.logger.error('user agregate: ' + id);
        } else {
          this.logger.error('user duplicate: ' + id);
        }
      }
    });

    return 'file users have been added';
  }
}
