import {
  BadRequestException,
  Body,
  Controller,
  Get,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Post,
  PreconditionFailedException,
  Put,
  Delete,
  Req,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';

import { GetUserService } from '../../application/get-data.service';
import { CreateUsersService } from '../../application/register-new-file.service';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('user')
export class UserController {
  constructor(
    private createSerivce: CreateUsersService,
    private getUserService: GetUserService,
  ) {}

  @Get('getInfo/:id')
  getAll(@Param('id') id) {
    return this.getUserService.run(id);
  }

  @Post('uploadFile')
  @UseInterceptors(FileInterceptor('file'))
  receiveDocuments(@UploadedFile() file: Express.Multer.File) {
    return this.createSerivce.run(file);
  }
}
