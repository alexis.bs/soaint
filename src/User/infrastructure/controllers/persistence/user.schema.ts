import { newUserInterface } from '../../../domain/entities/user-interface';
import { Document, Schema } from 'mongoose';

export type UserDocument = newUserInterface & Document;

export const UserSchema = new Schema<UserDocument>(
  {
    idNumber: { type: Number, required: true, unique: true },
    name: { type: String, required: true, unique: false },
    age: { type: Number, required: true, unique: false },
    gender: { type: String, required: true, unique: false },
  },
  {
    timestamps: {},
  },
);
