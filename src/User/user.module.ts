import { Module } from '@nestjs/common';
import { UserController } from './infrastructure/controllers/user.controller';
import { GetUserService } from './application/get-data.service';
import { UserSessionName } from './domain/entities/user-interface';
import { CreateUsersService } from './application/register-new-file.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './infrastructure/controllers/persistence/user.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: UserSessionName, schema: UserSchema }]),
  ],
  controllers: [UserController],
  providers: [GetUserService, CreateUsersService],
})
export class UserModule {}
